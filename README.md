# generator-jhipster-blog
[![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> JHipster module, Quick and simple blog

> See : https://framagit.org/generator-jhipster-blog/generator-jhipster-blog-root

# Introduction

This is a [JHipster](https://www.jhipster.tech/) module, that is meant to be used in a JHipster application.

# Prerequisites

As this is a [JHipster](https://www.jhipster.tech/) module, we expect you have JHipster and its related tools already installed:

- [Installing JHipster](https://www.jhipster.tech/installation/)

Mandatory dependencies :
- Sql database
- MapStruct
- AngularX
- I18n (en|fr)

This module was test with this configuration :
- Application : Monolithic
- Database : MariaDb and H2
- Maven
- Angular8
- I18n enabled
- ehCache

# Installation

## With NPM

To install this module:

```bash
npm install -g generator-jhipster-blog
```

To update this module:

```bash
npm update -g generator-jhipster-blog
```

## With Yarn

To install this module:

```bash
yarn global add generator-jhipster-blog
```

To update this module:

```bash
yarn global upgrade generator-jhipster-blog
```

# Usage
## With Yeoman
```bash
yo jhipster-blog
```

# License

MIT © [Gael S.](http://sigogneau.fr)


[npm-image]: https://img.shields.io/npm/v/generator-jhipster-blog.svg
[npm-url]: https://npmjs.org/package/generator-jhipster-blog
[travis-image]: https://travis-ci.org/gantispam/generator-jhipster-blog.svg?branch=master
[travis-url]: https://travis-ci.org/gantispam/generator-jhipster-blog
[daviddm-image]: https://david-dm.org/gantispam/generator-jhipster-blog.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/gantispam/generator-jhipster-blog
