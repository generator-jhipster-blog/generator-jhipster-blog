import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IComment } from '../../shared/model/comment.model';
import { CommentService } from '../../service/comment.service';

@Component({
  selector: 'jhi-blog-admin-comment-delete-dialog',
  templateUrl: './comment-delete-dialog-admin.component.html'
})
export class CommentDeleteDialogAdminComponent {
  comment: IComment;

  constructor(protected commentService: CommentService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.commentService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'commentListModification',
        content: 'Deleted an comment'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-blog-admin-comment-delete-popup',
  template: ''
})
export class CommentDeletePopupAdminComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ comment }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CommentDeleteDialogAdminComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.comment = comment;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/blog/admin/comment', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/blog/admin/comment', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
