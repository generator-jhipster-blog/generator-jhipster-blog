import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { <%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule } from 'app/shared/shared.module';

import { commentPublicRoute } from './comment-public.route';
import { CommentListPublicComponent } from './comment-list-public.component';
import { CommentCreatePublicComponent } from './comment-create-public.component';

const ENTITY_STATES = [...commentPublicRoute];

@NgModule({
  imports: [<%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [CommentListPublicComponent, CommentCreatePublicComponent],
  exports: [CommentListPublicComponent, CommentCreatePublicComponent],
  entryComponents: []
})
export class JhipsterCommentPublicModule {}
