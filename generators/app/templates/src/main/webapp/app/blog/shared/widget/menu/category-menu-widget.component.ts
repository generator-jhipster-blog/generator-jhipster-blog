import { Component } from '@angular/core';

import { CategoryService } from '../../../service/category.service';
import { ICategory } from '../../../shared/model/category.model';

/**
 * Widget for redirect on category section.
 */
@Component({
  selector: 'jhi-blog-widget-category-menu',
  templateUrl: './category-menu-widget.component.html'
})
export class CategoryMenuWidgetComponent {
  categories: ICategory[];

  constructor(protected categoryService: CategoryService) {}

  getService(): CategoryService {
    return this.categoryService;
  }

  trackId(index: number, item: ICategory) {
    return item.id;
  }

  loadCategory(dataCategories: ICategory[]) {
    this.categories = dataCategories;
  }

  onError(errorMessage: string) {
    console.error(errorMessage);
  }
}
