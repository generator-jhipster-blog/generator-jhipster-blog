import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { <%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule } from 'app/shared/shared.module';

import { PostDetailPublicComponent } from './post-detail-public.component';
import { postPublicRoute } from './post-public.route';
import { PostListPublicComponent } from './post-list-public.component';
import { JhipsterBlogSharedModule } from '../../shared/blog.shared.module';
import { JhipsterCommentPublicModule } from 'app/blog/public/comment/comment.public.module';

const ENTITY_STATES = [...postPublicRoute];

@NgModule({
  imports: [<%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule, JhipsterBlogSharedModule, RouterModule.forChild(ENTITY_STATES), JhipsterCommentPublicModule],
  declarations: [PostDetailPublicComponent, PostListPublicComponent],
  exports: []
})
export class JhipsterPostPublicModule {}
