import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { <%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule } from 'app/shared/shared.module';

import { CommentListAdminComponent } from './comment-list-admin.component';
import { CommentAdminComponent } from './comment-admin.component';
import { CommentUpdateAdminComponent } from './comment-update-admin.component';
import { CommentDeleteDialogAdminComponent, CommentDeletePopupAdminComponent } from './comment-delete-dialog-admin.component';
import { commentAdminPopupRoute, commentAdminRoute } from './comment-admin.route';
import { JhipsterBlogSharedModule } from '../../shared/blog.shared.module';
import { CommentDeleteSimpleDialogComponent } from './comment-delete-simple-dialog.component';

const ENTITY_STATES = [...commentAdminRoute, ...commentAdminPopupRoute];

@NgModule({
  imports: [<%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule, RouterModule.forChild(ENTITY_STATES), JhipsterBlogSharedModule],
  declarations: [
    CommentAdminComponent,
    CommentListAdminComponent,
    CommentUpdateAdminComponent,
    CommentDeleteDialogAdminComponent,
    CommentDeletePopupAdminComponent,
    CommentDeleteSimpleDialogComponent
  ],
  entryComponents: [CommentDeleteDialogAdminComponent, CommentDeleteSimpleDialogComponent]
})
export class JhipsterCommentAdminModule {}
