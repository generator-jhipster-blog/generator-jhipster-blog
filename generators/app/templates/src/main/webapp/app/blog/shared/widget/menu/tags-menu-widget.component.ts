import { Component } from '@angular/core';

import { TagsService } from '../../../service/tags.service';
import { ITags } from '../../../shared/model/tags.model';

/**
 * Widget for redirect on tags section.
 */
@Component({
  selector: 'jhi-blog-widget-tags-menu',
  templateUrl: './tags-menu-widget.component.html'
})
export class TagsMenuWidgetComponent {
  tags: ITags[];

  constructor(protected tagsService: TagsService) {}

  getService(): TagsService {
    return this.tagsService;
  }

  trackId(index: number, item: ITags) {
    return item.id;
  }

  loadTags(dataTags: ITags[]) {
    this.tags = dataTags;
  }

  onError(errorMessage: string) {
    console.error(errorMessage);
  }
}
