import { Moment } from 'moment';

export interface IComment {
  id?: number;
  username?: string;
  commentDate?: Moment;
  content?: any;
  postSlug?: string;
  postTitle?: string;
  postId?: number;
}

export class Comment implements IComment {
  constructor(
    public id?: number,
    public username?: string,
    public commentDate?: Moment,
    public content?: any,
    public postSlug?: string,
    public postTitle?: string,
    public postId?: number
  ) {}
}
