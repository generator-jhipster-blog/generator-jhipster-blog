import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { CommentService } from '../../service/comment.service';

@Component({
  selector: 'jhi-blog-admin-comment-delete-simple-dialog',
  templateUrl: './comment-delete-simple-dialog.component.html'
})
export class CommentDeleteSimpleDialogComponent {
  commentId: number;

  constructor(private commentService: CommentService, public activeModal: NgbActiveModal) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete() {
    this.commentService.delete(this.commentId).subscribe(response => {
      this.activeModal.close(true);
      window.history.back();
    });
  }
}
