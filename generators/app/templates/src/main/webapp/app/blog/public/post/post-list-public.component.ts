import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'jhi-blog-public-post-list',
  templateUrl: './post-list-public.component.html'
})
export class PostListPublicComponent {
  routeData: any;
  isLoading: boolean;
  requestParam: any;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router) {
    this.isLoading = false;
    this.requestParam = {};

    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.requestParam = Object.assign({}, this.activatedRoute.snapshot.queryParams);
      this.requestParam.page = data.pagingParams.page;
      this.requestParam.previousPage = data.pagingParams.page;
      this.requestParam.reverse = data.pagingParams.ascending;
      this.requestParam.predicate = data.pagingParams.predicate;
    });
  }

  transition(routeRequest: any) {
    this.router.navigate(['/blog'], { queryParams: routeRequest });
  }

  clear() {
    this.requestParam = {};
    this.requestParam.page = 1;
    this.router.navigate(['/blog'], { queryParams: this.requestParam });
  }

  // sort() {
  //   const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
  //   if (this.predicate !== 'id') {
  //     result.push('id');
  //   }
  //   return result;
  // }

  onSearchTerm(searchQuery: any) {
    this.buildRequest(searchQuery);
  }

  onFilterCategory(searchQuery: any) {
    this.buildRequest(searchQuery);
  }

  onFilterTags(searchQuery: any) {
    this.buildRequest(searchQuery);
  }

  private buildRequest(searchQuery: any) {
    // fix : force angular to detect object change
    this.requestParam = Object.assign({}, searchQuery);
    this.transition(searchQuery);
  }

  getCategoryName(): string {
    return decodeURIComponent(this.requestParam['categoryName.equals']).replace('—', '');
  }
}
