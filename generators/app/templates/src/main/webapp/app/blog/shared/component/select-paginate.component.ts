import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { JhiParseLinks } from 'ng-jhipster';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';

@Component({
  selector: 'jhi-blog-select-paginate',
  templateUrl: './select-paginate.component.html'
})
export class SelectPaginateComponent implements OnInit {
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  pageTotal: number;

  @Input() service: { query };
  @Input() method: string;
  @Input() sortBy: any[];

  @Output() loadDataEvent = new EventEmitter<any>();
  @Output() errorEvent = new EventEmitter<any>();

  constructor(protected parseLinks: JhiParseLinks) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 1;
    this.totalItems = 1;
  }

  ngOnInit() {
    this.loadAll();
  }

  loadAll() {
    const queryRequest: any = {};
    queryRequest.page = this.page - 1;
    queryRequest.size = this.itemsPerPage;
    if (undefined !== this.sortBy && null !== this.sortBy) {
      queryRequest.sort = this.sortBy;
    }
    this.service[this.method](queryRequest).subscribe(
      (res: HttpResponse<any[]>) => this.paginate(res.body, res.headers),
      (res: HttpErrorResponse) => this.postErrorEvent(res.message)
    );
  }

  protected paginate(data: any[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.computeTotalPage();
    this.postDataEvent(data);
  }

  postDataEvent(data: any[]) {
    this.loadDataEvent.emit(data);
  }

  postErrorEvent(message: string) {
    this.errorEvent.emit(message);
  }

  onClickEvent(nextPage: number) {
    this.page = this.page + nextPage;
    this.loadAll();
  }

  haveNextPage(): boolean {
    return this.page * this.itemsPerPage < this.totalItems;
  }

  computeTotalPage() {
    this.pageTotal = Math.ceil(this.totalItems / this.itemsPerPage);
  }
}
