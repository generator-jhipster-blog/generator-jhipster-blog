import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { Comment, IComment } from '../../shared/model/comment.model';
import { CommentService } from '../../service/comment.service';
import { CommentCreatePublicComponent } from './comment-create-public.component';
import { CommentListPublicComponent } from './comment-list-public.component';

@Injectable({ providedIn: 'root' })
export class CommentPublicResolve implements Resolve<IComment[]> {
  constructor(private service: CommentService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IComment[]> {
    const postId = route.params['postId'];
    if (postId) {
      this.service.queryByPostId(postId).subscribe(todo => console.error(todo));
      return this.service.queryByPostId(postId).pipe(map((comment: HttpResponse<Comment[]>) => comment.body));
    }
    return of([]);
  }
}

export const commentPublicRoute: Routes = [
  {
    path: 'post/:postId',
    component: CommentListPublicComponent,
    resolve: {
      comment: CommentPublicResolve
    },
    data: {
      authorities: [],
      pageTitle: '<%= angularAppName %>.comment.home.title'
    }
  },
  {
    path: 'new',
    component: CommentCreatePublicComponent,
    data: {
      authorities: [],
      pageTitle: '<%= angularAppName %>.comment.home.title'
    }
  }
];
