import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'jhi-blog-widget-common-search',
  templateUrl: './common-search-widget.component.html'
})
export class CommonSearchWidgetComponent {
  /** Page is loading ? */
  @Input() isDataLoading: boolean;

  /** Search string */
  @Input() search: string;
  @Output() searchChange = new EventEmitter<string>();

  /** Reset search event */
  @Output() resetEvent = new EventEmitter<string>();

  constructor() {
    this.search = null;
  }

  onSubmitEvent() {
    if (null === this.search || '' === this.search) {
      this.onResetEvent();
      return;
    }
    this.searchChange.emit(this.search);
  }

  onResetEvent() {
    this.search = null;
    this.resetEvent.emit();
  }
}
