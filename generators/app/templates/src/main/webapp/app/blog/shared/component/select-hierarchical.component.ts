import { Component, Input } from '@angular/core';
import { JhiParseLinks } from 'ng-jhipster';

/* tslint:disable:component-selector */
@Component({
  selector: 'select[jhi-blog-select-hierarchical]',
  templateUrl: './select-hierarchical.component.html'
})
export class SelectHierarchicalComponent {
  /** (optional) add empty option in selectbox ? */
  @Input() optional: boolean;
  /** (optional) map : key on ngValue. by default : ID */
  @Input() itemKey: any;
  /** (mandatory) map : value to display */
  @Input() itemValue: any;
  /** (mandatory) array with data */
  @Input() datas: { id: number; childrens: any[] }[];
  /** (optional) pre-selected item on list */
  @Input() selectedItemId: number;

  constructor(protected parseLinks: JhiParseLinks) {
    this.optional = false;
    this.datas = [];
    this.itemKey = 'id';
    this.selectedItemId = null;
  }

  trackId(index: number, item: any) {
    return item.id;
  }
}
