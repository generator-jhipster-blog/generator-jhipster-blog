import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IAngularEditorConfig } from '@gsig/angular-editor-fa';

import { ITags, Tags } from '../../shared/model/tags.model';
import { TagsService } from '../../service/tags.service';
import { SlugUtil } from '../../shared/util/slug-util';
import { REGEX_SLUG } from '../../blog.constants';
import { WysiwygUtil } from '../../shared/util/wysiwyg-util';

// TODO FIXME : wysig

@Component({
  selector: 'jhi-blog-admin-tags-update',
  templateUrl: './tags-update-admin.component.html',
  styles: WysiwygUtil.configSimpleCss()
})
export class TagsUpdateAdminComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.minLength(3)]],
    slug: [null, [Validators.required, Validators.minLength(3), Validators.pattern(REGEX_SLUG)]],
    description: []
  });

  editorConfig: IAngularEditorConfig = WysiwygUtil.configSimple();

  @Output() createdTags = new EventEmitter<any>();

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected tagsService: TagsService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ tags }) => {
      this.updateForm(tags);
    });
  }

  updateForm(tags: ITags) {
    this.editForm.patchValue({
      id: tags.id,
      name: tags.name,
      slug: tags.slug,
      description: tags.description
    });
  }

  save() {
    this.isSaving = true;
    const tags = this.createFromForm();
    if (tags.id !== undefined && tags.id !== null) {
      this.subscribeToSaveResponse(this.tagsService.update(tags), tags.id);
    } else {
      this.subscribeToSaveResponse(this.tagsService.create(tags), tags.id);
    }
  }

  private createFromForm(): ITags {
    return {
      ...new Tags(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      slug: this.editForm.get(['slug']).value,
      description: this.editForm.get(['description']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITags>>, tagId: number) {
    result.subscribe(() => this.onSaveSuccess(tagId), () => this.onSaveError());
  }

  buildSlug() {
    this.editForm.controls['slug'].setValue(SlugUtil.slugify(this.editForm.controls['name'].value));
  }

  protected onSaveSuccess(tagId: number) {
    this.isSaving = false;
    this.createdTags.emit();
    this.resetFrom();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  resetFrom() {
    this.editForm.reset();
  }
}
