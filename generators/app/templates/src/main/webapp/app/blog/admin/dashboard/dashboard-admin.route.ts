import { Routes } from '@angular/router';
import { DashboardAdminComponent } from './dashboard-admin.component';

export const dashboardAdminRoute: Routes = [
  {
    path: '',
    component: DashboardAdminComponent,
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: '<%= angularAppName %>.menu.dashboard'
    }
  }
];
