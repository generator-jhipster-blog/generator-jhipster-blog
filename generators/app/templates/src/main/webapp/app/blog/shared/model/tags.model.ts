export interface ITags {
  id?: number;
  name?: string;
  slug?: string;
  description?: any;
  selectedItem?: boolean;
}

export class Tags implements ITags {
  constructor(public id?: number, public name?: string, public slug?: string, public description?: any, public selectedItem?: boolean) {
    this.selectedItem = false;
  }
}
