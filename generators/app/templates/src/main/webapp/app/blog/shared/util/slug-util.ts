/**
 * Slug utility.
 */
export class SlugUtil {
  /**
   * Slugify a string.
   *
   * @param stringToSlugify
   */
  static slugify(stringToSlugify: string): string {
    if (stringToSlugify != null) {
      return stringToSlugify
        .toLowerCase() // lower case
        .trim() // Remove space from start and end of text
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '') // replace special char
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(/&/g, '-and-') // Replace & with 'and'
        .replace(/[^\w-]+/g, '') // Remove all non-word characters
        .replace(/--+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, ''); // Trim - from end of text
    }
    return null;
  }
}
