import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AngularEditorFaModule } from '@gsig/angular-editor-fa';

import { <%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule } from 'app/shared/shared.module';

import { TagsAdminComponent } from './tags-admin.component';
import { TagsUpdateAdminComponent } from './tags-update-admin.component';
import { TagsDeleteDialogAdminComponent, TagsDeletePopupAdminComponent } from './tags-delete-dialog-admin.component';
import { tagsAdminRoute, tagsPopupAdminRoute } from './tags-admin.route';
import { TagsListAdminComponent } from './tags-list-admin.component';
import { JhipsterBlogSharedModule } from '../../shared/blog.shared.module';

const ENTITY_STATES = [...tagsAdminRoute, ...tagsPopupAdminRoute];

@NgModule({
  imports: [<%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule, RouterModule.forChild(ENTITY_STATES), JhipsterBlogSharedModule, AngularEditorFaModule],
  declarations: [
    TagsAdminComponent,
    TagsListAdminComponent,
    TagsUpdateAdminComponent,
    TagsDeleteDialogAdminComponent,
    TagsDeletePopupAdminComponent
  ],
  entryComponents: [TagsDeleteDialogAdminComponent]
})
export class JhipsterTagsAdminModule {}
