import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { <%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule } from 'app/shared/shared.module';

import { TagsDetailPublicComponent } from './tags-detail-public.component';
import { tagsPublicRoute } from './tags-public.route';
import { JhipsterBlogSharedModule } from '../../shared/blog.shared.module';
import { TagsListPublicComponent } from './tags-list-public.component';

const ENTITY_STATES = [...tagsPublicRoute];

@NgModule({
  imports: [<%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule, JhipsterBlogSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [TagsDetailPublicComponent, TagsListPublicComponent]
})
export class JhipsterTagsPublicModule {}
