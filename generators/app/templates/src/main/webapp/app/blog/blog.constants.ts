export const REGEX_SLUG = '^[a-z0-9]+(?:-[a-z0-9]+)*$';
export const ITEMS_PER_PAGE_SMALL = 10;
