import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { JhiEventManager } from 'ng-jhipster';

import { IPost } from '../../shared/model/post.model';
import { PostService } from '../../service/post.service';

@Component({
  selector: 'jhi-blog-admin-post-delete-dialog',
  templateUrl: './post-delete-dialog-admin.component.html'
})
export class PostDeleteDialogAdminComponent {
  post: IPost;

  constructor(protected postService: PostService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(slug: number) {
    this.postService.delete(slug).subscribe(response => {
      this.eventManager.broadcast({
        name: 'postListModification',
        content: 'Deleted an post'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-blog-admin-post-delete-popup',
  template: ''
})
export class PostDeletePopupAdminComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ post }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(PostDeleteDialogAdminComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.post = post;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/blog/admin/post', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/blog/admin/post', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
