import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';

import { ITags } from '../shared/model/tags.model';
import { ISearch } from '../shared/model/search.model';

type EntityResponseType = HttpResponse<ITags>;
type EntityArrayResponseType = HttpResponse<ITags[]>;

@Injectable({ providedIn: 'root' })
export class TagsService {
  public resourceUrl = SERVER_API_URL + 'api/blog/tags';
  public resourceUrlPublic = SERVER_API_URL + 'api/blog/public/tags';

  constructor(protected http: HttpClient) {}

  create(tags: ITags): Observable<EntityResponseType> {
    return this.http.post<ITags>(this.resourceUrl, tags, { observe: 'response' });
  }

  update(tags: ITags): Observable<EntityResponseType> {
    return this.http.put<ITags>(this.resourceUrl, tags, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITags>(`${this.resourceUrlPublic}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITags[]>(this.resourceUrlPublic, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  searchFull(searchTerm: ISearch, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.post<ITags[]>(this.resourceUrlPublic + '/search', searchTerm, {
      params: options,
      observe: 'response'
    });
  }
}
