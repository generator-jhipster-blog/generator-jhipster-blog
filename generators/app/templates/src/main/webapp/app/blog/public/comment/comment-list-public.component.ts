import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { AccountService } from 'app/core/auth/account.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';

import { IComment } from '../../shared/model/comment.model';
import { CommentService } from '../../service/comment.service';

@Component({
  selector: 'jhi-blog-public-comment-list',
  templateUrl: './comment-list-public.component.html'
})
export class CommentListPublicComponent implements OnInit, OnDestroy {
  comments: IComment[];
  commentListSubscription: Subscription;
  currentAccount: any;
  itemsPerPage: number;
  links: any;
  page: any;
  predicate: any;
  reverse: any;
  totalItems: number;

  @Input() postId: number;

  constructor(
    protected commentService: CommentService,
    protected eventManager: JhiEventManager,
    protected parseLinks: JhiParseLinks,
    protected accountService: AccountService
  ) {
    this.comments = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.reverse = true;
  }

  loadAll() {
    this.commentService
      .queryByPostId(this.postId, {
        page: this.page,
        size: this.itemsPerPage,
        sort: ['id,desc']
      })
      .subscribe((res: HttpResponse<IComment[]>) => this.paginateComments(res.body, res.headers));
  }

  reset() {
    this.page = 0;
    this.comments = [];
    this.loadAll();
  }

  loadPage(page) {
    this.page = page;
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
      this.registerChangeInComments();
    });
  }

  ngOnDestroy() {
    if (this.commentListSubscription) {
      this.eventManager.destroy(this.commentListSubscription);
    }
  }

  registerChangeInComments() {
    this.commentListSubscription = this.eventManager.subscribe('commentListModification', response => this.reset());
  }

  trackId(index: number, item: IComment) {
    return item.id;
  }

  protected paginateComments(data: IComment[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    for (let i = 0; i < data.length; i++) {
      this.comments.push(data[i]);
    }
  }
}
