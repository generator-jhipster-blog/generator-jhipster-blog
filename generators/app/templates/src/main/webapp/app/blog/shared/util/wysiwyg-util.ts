import { IAngularEditorConfig } from '@gsig/angular-editor-fa';

/**
 * Wysiwyg util.
 * @see https://www.npmjs.com/package/@kolkov/angular-editor
 */
export class WysiwygUtil {
  /**
   * Simple configuration.
   */
  static configSimple(): IAngularEditorConfig {
    return {
      editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '100',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: '',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      uploadUrl: 'v1/image',
      sanitize: true
    };
  }

  /**
   * Simple configuration.
   */
  static configFull(): IAngularEditorConfig {
    return {
      editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '400',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      sanitize: true,
      toolbarPosition: 'top'
    };
  }

  static configSimpleCss(): any[] {
    return [
      `
      :host ::ng-deep angular-editor #insertImage-field_description {
        visibility: hidden;
        display: none;
      }

      :host ::ng-deep angular-editor #insertVideo-field_description {
        visibility: hidden;
        display: none;
      }
    `
    ];
  }
}
