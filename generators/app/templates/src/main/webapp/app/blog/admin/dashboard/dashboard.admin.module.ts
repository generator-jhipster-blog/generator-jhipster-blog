import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { <%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule } from 'app/shared/shared.module';

import { dashboardAdminRoute } from './dashboard-admin.route';
import { DashboardAdminComponent } from './dashboard-admin.component';
import { JhipsterBlogSharedModule } from '../../shared/blog.shared.module';

const ENTITY_STATES = [...dashboardAdminRoute];

@NgModule({
  imports: [<%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule, RouterModule.forChild(ENTITY_STATES), JhipsterBlogSharedModule],
  declarations: [DashboardAdminComponent],
  entryComponents: []
})
export class JhipsterDashboardAdminModule {}
