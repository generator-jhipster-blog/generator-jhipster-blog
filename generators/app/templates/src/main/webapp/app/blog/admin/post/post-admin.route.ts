import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

import { IPost, Post } from '../../shared/model/post.model';
import { PostService } from '../../service/post.service';
import { PostUpdateAdminComponent } from './post-update-admin.component';
import { PostDeletePopupAdminComponent } from './post-delete-dialog-admin.component';
import { PostAdminComponent } from './post-admin.component';

@Injectable({ providedIn: 'root' })
export class PostAdminResolve implements Resolve<IPost> {
  constructor(private service: PostService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPost> {
    const slug = route.params['slug'];
    if (slug) {
      return this.service.find(slug).pipe(
        filter((response: HttpResponse<Post>) => response.ok),
        map((post: HttpResponse<Post>) => post.body)
      );
    }
    return of(new Post());
  }
}

export const postAdminRoute: Routes = [
  {
    path: '',
    component: PostAdminComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'createdDate,desc',
      pageTitle: '<%= angularAppName %>.post.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PostUpdateAdminComponent,
    resolve: {
      post: PostAdminResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: '<%= angularAppName %>.post.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':slug/edit',
    component: PostUpdateAdminComponent,
    resolve: {
      post: PostAdminResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: '<%= angularAppName %>.post.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const postPopupAdminRoute: Routes = [
  {
    path: ':slug/delete',
    component: PostDeletePopupAdminComponent,
    resolve: {
      post: PostAdminResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: '<%= angularAppName %>.post.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
