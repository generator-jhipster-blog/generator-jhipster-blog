export interface ISearch {
  fullTerm?: string;
}

export class Search implements ISearch {
  constructor(public fullTerm?: string) {}
}
