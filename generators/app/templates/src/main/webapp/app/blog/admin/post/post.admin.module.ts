import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AngularEditorFaModule } from '@gsig/angular-editor-fa';

import { <%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule } from 'app/shared/shared.module';

import { PostListAdminComponent } from './post-list-admin.component';
import { PostUpdateAdminComponent } from './post-update-admin.component';
import { PostDeleteDialogAdminComponent, PostDeletePopupAdminComponent } from './post-delete-dialog-admin.component';
import { postAdminRoute, postPopupAdminRoute } from './post-admin.route';
import { JhipsterBlogSharedModule } from '../../shared/blog.shared.module';
import { PostAdminComponent } from './post-admin.component';

const ENTITY_STATES = [...postAdminRoute, ...postPopupAdminRoute];

@NgModule({
  imports: [<%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule, RouterModule.forChild(ENTITY_STATES), JhipsterBlogSharedModule, AngularEditorFaModule],
  declarations: [
    PostAdminComponent,
    PostListAdminComponent,
    PostUpdateAdminComponent,
    PostDeleteDialogAdminComponent,
    PostDeletePopupAdminComponent
  ],
  entryComponents: [PostDeleteDialogAdminComponent]
})
export class JhipsterPostAdminModule {}
