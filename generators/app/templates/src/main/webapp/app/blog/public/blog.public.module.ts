import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'tags',
        loadChildren: () => import('./tags/tags.public.module').then(m => m.JhipsterTagsPublicModule)
      },
      {
        path: 'category',
        loadChildren: () => import('./category/category.public.module').then(m => m.JhipsterCategoryPublicModule)
      },
      {
        path: 'post',
        loadChildren: () => import('./post/post.public.module').then(m => m.JhipsterPostPublicModule)
      },
      {
        path: '',
        loadChildren: () => import('./post/post.public.module').then(m => m.JhipsterPostPublicModule)
      },
      {
        path: 'comment',
        loadChildren: () => import('./comment/comment.public.module').then(m => m.JhipsterCommentPublicModule)
      }
    ])
  ]
})
export class JhipsterBlogPublicModule {}
