import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { <%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule } from 'app/shared/shared.module';

import { CategoryDetailPublicComponent } from './category-detail-public.component';
import { categoryPublicRoute } from './category-public.route';
import { JhipsterBlogSharedModule } from '../../shared/blog.shared.module';
import { CategoryListPublicComponent } from './category-list-public.component';

const ENTITY_STATES = [...categoryPublicRoute];

@NgModule({
  imports: [<%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule, JhipsterBlogSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [CategoryDetailPublicComponent, CategoryListPublicComponent]
})
export class JhipsterCategoryPublicModule {}
