import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ICategory } from '../../shared/model/category.model';

@Component({
  selector: 'jhi-blog-public-category-detail',
  templateUrl: './category-detail-public.component.html'
})
export class CategoryDetailPublicComponent implements OnInit {
  category: ICategory;
  requestParam: any;
  routeData: any;

  constructor(protected router: Router, protected activatedRoute: ActivatedRoute) {
    this.requestParam = {};
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ category }) => {
      this.category = category;
      // fix : force angular detection when object change
      this.requestParam = Object.assign({}, this.requestParam);
      this.requestParam['categoryId.equals'] = category.id;
    });
  }

  transition(routeRequest: any) {
    this.requestParam = routeRequest;
    this.router.navigate(['/blog/category', this.category.slug], { queryParams: routeRequest });
  }

  previousState() {
    window.history.back();
  }
}
