import { Component, EventEmitter, Input, Output } from '@angular/core';

import { CategoryService } from '../../../service/category.service';
import { ICategory } from '../../../shared/model/category.model';

/**
 * Widget for filtering current by category.
 */
@Component({
  selector: 'jhi-blog-widget-category-filter',
  templateUrl: './category-filter-widget.component.html'
})
export class CategoryFilterWidgetComponent {
  @Input() queryRequest: any;

  @Output() onCategoryChange = new EventEmitter<any>();

  categories: ICategory[];

  constructor(protected categoryService: CategoryService) {}

  getService(): CategoryService {
    return this.categoryService;
  }

  trackId(index: number, item: ICategory) {
    return item.id;
  }

  onSelectCategory(category: ICategory) {
    this.queryRequest['categoryId.equals'] = category.id;
    this.queryRequest['categoryName.equals'] = encodeURIComponent(category.name);
    this.onCategoryChange.emit(this.queryRequest);
  }

  onSelectAllCategory() {
    delete this.queryRequest['categoryId.equals'];
    delete this.queryRequest['categoryName.equals'];
    this.onCategoryChange.emit(this.queryRequest);
  }

  isActiveItem(categoryId: number): boolean {
    return this.queryRequest['categoryId.equals'] && parseInt(this.queryRequest['categoryId.equals'], 10) === categoryId;
  }

  loadCategory(dataCategories: ICategory[]) {
    this.categories = dataCategories;
  }

  onError(errorMessage: string) {
    console.error(errorMessage);
  }
}
