import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'jhi-blog-widget-post-search',
  templateUrl: './post-search-widget.component.html'
})
export class PostSearchWidgetComponent {
  /** Page is loading ? */
  @Input() isDataLoading: boolean;

  private _query: any;

  get query(): any {
    return this._query;
  }

  /**
   * REST request parameters.
   * Search/Filtering instructions.
   */
  @Input() set query(value: any) {
    if (value) {
      this._query = value;
      this.search = value['title.contains'];
    } else {
      this._query = {};
    }
  }

  @Output() queryChange = new EventEmitter<any>();

  /** Route request for search event */
  search: string;

  /** on submit event */
  @Output() onSearchEvent = new EventEmitter<any>();

  constructor() {
    this.search = null;
  }

  onSubmitEvent() {
    if (null === this.search || '' === this.search.trim()) {
      this.onResetEvent();
      return;
    }
    const query = Object.assign({}, this._query);
    query['title.contains'] = this.search;
    query['description.contains'] = this.search;
    query['content.contains'] = this.search;
    query['createdBy.contains'] = this.search;
    query['mode'] = 'filter';
    this.onSearchEvent.emit(query);
  }

  onResetEvent() {
    this.search = null;
    const query = Object.assign({}, this._query);
    delete query['title.contains'];
    delete query['description.contains'];
    delete query['content.contains'];
    delete query['createdBy.contains'];
    delete query['mode'];
    this.onSearchEvent.emit(query);
  }
}
