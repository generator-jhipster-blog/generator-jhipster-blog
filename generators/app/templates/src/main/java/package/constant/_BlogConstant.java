package <%=packageName%>.constant;

public class BlogConstant {

    /** Slug format */
    public static final String REGEX_SLUG = "^[a-z0-9]+(?:-[a-z0-9]+)*$";

}
