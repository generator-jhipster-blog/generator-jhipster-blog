package <%=packageName%>.service.blog.dto;

import <%=packageName%>.constant.BlogConstant;
import <%=packageName%>.domain.blog.Category;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link Category} entity.
 */
public class CategoryDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 3)
    private String name;

    @NotNull
    @Size(min = 3)
    @Pattern(regexp = BlogConstant.REGEX_SLUG)
    private String slug;

    @Lob
    private String description;

    @ApiModelProperty(value = "childrens")
    private Set<CategoryDTO> childrens = new HashSet<>();

    private Long parentCategoryId;

    private String parentCategoryName;

    private String parentCategorySlug;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Long categoryId) {
        this.parentCategoryId = categoryId;
    }

    public String getParentCategoryName() {
        return parentCategoryName;
    }

    public void setParentCategoryName(String categoryName) {
        this.parentCategoryName = categoryName;
    }

    public String getParentCategorySlug() {
        return parentCategorySlug;
    }

    public void setParentCategorySlug(String parentCategorySlug) {
        this.parentCategorySlug = parentCategorySlug;
    }

    public Set<CategoryDTO> getChildrens() {
        return childrens;
    }

    public void setChildrens(Set<CategoryDTO> childrens) {
        this.childrens = childrens;
    }

    public void addChildrens(CategoryDTO childrens) {
        this.childrens.add(childrens);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CategoryDTO categoryDTO = (CategoryDTO) o;
        if (categoryDTO.getId() == null || getId() == null) {
            return false;
        }
        if (Objects.equals(getId(), categoryDTO.getId())) {
            return true;
        }
        return Objects.equals(getSlug(), categoryDTO.getSlug());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CategoryDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", slug='" + getSlug() + "'" +
            ", description='" + getDescription() + "'" +
            ", parentCategoryId=" + getParentCategoryId() +
            ", parentCategoryName='" + getParentCategoryName() + "'" +
            ", parentCategorySlug='" + getParentCategorySlug() + "'" +
            "}";
    }
}
