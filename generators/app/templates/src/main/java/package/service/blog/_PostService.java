package <%=packageName%>.service.blog;

import <%=packageName%>.domain.blog.Category;
import <%=packageName%>.domain.blog.Post;
import <%=packageName%>.domain.blog.Tags;
import <%=packageName%>.repository.blog.PostRepository;
import <%=packageName%>.security.AuthoritiesConstants;
import <%=packageName%>.security.SecurityUtils;
import <%=packageName%>.service.UserService;
import <%=packageName%>.service.blog.dto.PostDTO;
import <%=packageName%>.service.blog.mapper.PostMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Post}.
 */
@Service
@Transactional
public class PostService {

    private final Logger log = LoggerFactory.getLogger(PostService.class);

    private final PostRepository postRepository;

    private final PostMapper postMapper;

    private final UserService userService;

    public PostService(PostRepository postRepository, PostMapper postMapper, UserService userService) {
        this.postRepository = postRepository;
        this.postMapper = postMapper;
        this.userService = userService;
    }

    /**
     * Save a post.
     *
     * @param postDTO the entity to save.
     * @return the persisted entity.
     */
    public PostDTO save(PostDTO postDTO) {
        log.debug("Request to save Post : {}", postDTO);
        Post post = postMapper.toEntity(postDTO);
        post = postRepository.save(post);
        return postMapper.toDto(post);
    }

    /**
     * Get all the posts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PostDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Posts");
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            return postRepository.findAll(pageable).map(postMapper::toDto);
        }
        return postRepository.findAllByPublishIsTrue(pageable).map(postMapper::toDto);
    }

    /**
     * Get all the posts with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<PostDTO> findAllWithEagerRelationships(Pageable pageable) {
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            return postRepository.findAllWithEagerRelationships(pageable).map(postMapper::toDto);
        }
        return postRepository.findAllByPublisIsTrueWithEagerRelationships(pageable).map(postMapper::toDto);
    }

    /**
     * Get one post by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PostDTO> findOne(Long id) {
        log.debug("Request to get Post : {}", id);
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            return postRepository.findOneWithEagerRelationships(id).map(postMapper::toDto);
        }
        return postRepository.findOneByPublishIsTrueWithEagerRelationships(id).map(postMapper::toDto);
    }

    /**
     * Get one post by id.
     *
     * @param slug the slug of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PostDTO> findOne(String slug) {
        log.debug("Request to get Post : {}", slug);
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            return postRepository.findOneWithEagerRelationships(slug).map(postMapper::toDto);
        }
        return postRepository.findOneByPublishIsTrueWithEagerRelationships(slug).map(postMapper::toDto);
    }

    /**
     * Delete the post by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Post : {}", id);
        postRepository.deleteById(id);
    }

    /**
     * Remove a tag on all posts.
     *
     * @param tag to remove
     */
    public void removeTag(Tags tag) {
        log.debug("Request to delete Tag from Post : {}", tag);
        postRepository.findAllByTags(tag).forEach(t -> t.removeTags(tag));
    }

    /**
     * Remove a category on all posts.
     *
     * @param category to remove
     */
    public void removeCategory(Category category) {
        log.debug("Request to delete Category from Post : {}", category);
        postRepository.findAllByCategory(category).forEach(t -> t.setCategory(null));
    }

}
