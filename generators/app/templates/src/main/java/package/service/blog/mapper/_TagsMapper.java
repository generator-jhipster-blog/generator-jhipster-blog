package <%=packageName%>.service.blog.mapper;

import <%=packageName%>.domain.blog.Tags;
import <%=packageName%>.service.blog.dto.TagsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Tags} and its DTO {@link TagsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TagsMapper extends BlogEntityMapper<TagsDTO, Tags> {



    default Tags fromId(Long id) {
        if (id == null) {
            return null;
        }
        Tags tags = new Tags();
        tags.setId(id);
        return tags;
    }
}
