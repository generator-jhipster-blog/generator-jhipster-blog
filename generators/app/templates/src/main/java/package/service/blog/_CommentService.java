package <%=packageName%>.service.blog;

import <%=packageName%>.domain.blog.Comment;
import <%=packageName%>.repository.blog.CommentRepository;
import <%=packageName%>.security.SecurityUtils;
import <%=packageName%>.service.blog.dto.CommentDTO;
import <%=packageName%>.service.blog.mapper.CommentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Comment}.
 */
@Service
@Transactional
public class CommentService {

    private final Logger log = LoggerFactory.getLogger(CommentService.class);

    private final CommentRepository commentRepository;

    private final CommentMapper commentMapper;

    public CommentService(CommentRepository commentRepository, CommentMapper commentMapper) {
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
    }

    /**
     * Save a comment.
     *
     * @param commentDTO the entity to save.
     * @return the persisted entity.
     */
    public CommentDTO save(CommentDTO commentDTO) {
        log.debug("Request to save Comment : {}", commentDTO);
        buildCommment(commentDTO);
        Comment comment = commentMapper.toEntity(commentDTO);
        comment = commentRepository.save(comment);
        return commentMapper.toDto(comment);
    }

    /**
     * Set Created date for new entity.
     *
     * @param commentDTO to build
     */
    private void buildCommment(CommentDTO commentDTO) {
        if (null == commentDTO.getId()) {
            SecurityUtils.getCurrentUserLogin().ifPresent(commentDTO::setUsername);
            commentDTO.setCommentDate(Instant.now());
        }
        if (null != commentDTO.getId() && null == commentDTO.getCommentDate()) {
            findOne(commentDTO.getId()).ifPresent(r -> commentDTO.setCommentDate(r.getCommentDate()));
        }
    }

    /**
     * Get all the comments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CommentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Comments");
        return commentRepository.findAll(pageable).map(commentMapper::toDto);
    }

    /**
     * Get all the comments by post id.
     *
     * @param postId Post id
     * @param pageable Spring page
     * @return Spring page
     */
    public Page<CommentDTO> findAllByPost(Long postId, Pageable pageable) {
        log.debug("Request to get all Comments by postId={}", postId);
        return commentRepository.findAllByPostId(postId, pageable).map(commentMapper::toDto);
    }

    /**
     * Get one comment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CommentDTO> findOne(Long id) {
        log.debug("Request to get Comment : {}", id);
        return commentRepository.findById(id)
            .map(commentMapper::toDto);
    }

    /**
     * Delete the comment by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Comment : {}", id);
        commentRepository.deleteById(id);
    }

}
