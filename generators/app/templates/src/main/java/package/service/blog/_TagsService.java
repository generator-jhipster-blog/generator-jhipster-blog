package <%=packageName%>.service.blog;

import <%=packageName%>.domain.blog.Tags;
import <%=packageName%>.repository.blog.TagsRepository;
import <%=packageName%>.service.blog.dto.TagsDTO;
import <%=packageName%>.service.blog.mapper.TagsMapper;
import <%=packageName%>.web.rest.blog.vm.BlogSearchTermVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Tags}.
 */
@Service
@Transactional
public class TagsService {

    private final Logger log = LoggerFactory.getLogger(TagsService.class);

    private final TagsRepository tagsRepository;

    private final TagsMapper tagsMapper;

    private final PostService postService;

    public TagsService(TagsRepository tagsRepository, TagsMapper tagsMapper, PostService postService) {
        this.tagsRepository = tagsRepository;
        this.tagsMapper = tagsMapper;
        this.postService = postService;
    }

    /**
     * Save a tags.
     *
     * @param tagsDTO the entity to save.
     * @return the persisted entity.
     */
    public TagsDTO save(TagsDTO tagsDTO) {
        log.debug("Request to save Tags : {}", tagsDTO);
        Tags tags = tagsMapper.toEntity(tagsDTO);
        tags = tagsRepository.save(tags);
        return tagsMapper.toDto(tags);
    }

    /**
     * Is already exist?
     *
     * @param tagsDTO the entity to test.
     * @return yes or not
     */
    public boolean isExist(TagsDTO tagsDTO) {
        Optional<Tags> tagFound = tagsRepository.findOneByNameOrSlug(tagsDTO.getName(), tagsDTO.getSlug());
        return tagFound.filter(tags -> !tags.getId().equals(tagsDTO.getId())).isPresent();
    }

    /**
     * Get all the tags.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TagsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Tags");
        return tagsRepository.findAll(pageable).map(tagsMapper::toDto);
    }


    /**
     * Get one tags by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TagsDTO> findOne(Long id) {
        log.debug("Request to get Tags : {}", id);
        return tagsRepository.findById(id).map(tagsMapper::toDto);
    }

    /**
     * Get one tags by slug.
     *
     * @param slug the slug of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TagsDTO> findOne(String slug) {
        log.debug("Request to get Tags : {}", slug);
        return tagsRepository.findBySlug(slug).map(tagsMapper::toDto);
    }

    /**
     * Delete the tags by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Tags : {}", id);
        Optional<Tags> tagFound = tagsRepository.findById(id);
        if (tagFound.isPresent()) {
            postService.removeTag(tagFound.get());
            tagsRepository.deleteById(id);
        }
    }

    /**
     * Search on all attributs.
     *
     * @param searchTerm search term
     * @param pageable   spring page
     * @return spring page
     */
    public Page<TagsDTO> searchFull(BlogSearchTermVM searchTerm, Pageable pageable) {
        log.debug("Request to search all Tags with term {}", searchTerm);
        return tagsRepository.searchFull("%".concat(searchTerm.getFullTerm()).concat("%"), pageable).map(tagsMapper::toDto);
    }
}
