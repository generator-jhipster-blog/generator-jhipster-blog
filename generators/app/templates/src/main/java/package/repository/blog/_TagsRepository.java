package <%=packageName%>.repository.blog;

import <%=packageName%>.domain.blog.Tags;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the Tags entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TagsRepository extends JpaRepository<Tags, Long> {

    /**
     * Find by name or slug.
     *
     * @param name tag name
     * @param slug tag slug
     * @return optional tag
     */
    Optional<Tags> findOneByNameOrSlug(String name, String slug);

    Optional<Tags> findBySlug(String slug);

    @Query(value = "select distinct tags from Tags tags where tags.name like :term or tags.description like :term",
        countQuery = "select count(distinct tags) from Tags tags where tags.name like :term or tags.description like :term")
    Page<Tags> searchFull(@Param("term") String term, Pageable pageable);
}
