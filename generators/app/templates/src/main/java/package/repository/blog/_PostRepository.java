package <%=packageName%>.repository.blog;

import <%=packageName%>.domain.blog.Category;
import <%=packageName%>.domain.blog.Post;
import <%=packageName%>.domain.blog.Tags;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Spring Data  repository for the Post entity.
 */
@Repository
public interface PostRepository extends JpaRepository<Post, Long>, JpaSpecificationExecutor<Post> {

    @Query(value = "select distinct post from Post post left join fetch post.tags", countQuery = "select count(distinct post) from Post post")
    Page<Post> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct post from Post post left join fetch post.tags where post.publish=true", countQuery = "select count(distinct post) from Post post where post.publish=true")
    Page<Post> findAllByPublisIsTrueWithEagerRelationships(Pageable pageable);

    @Query("select distinct post from Post post left join fetch post.tags")
    List<Post> findAllWithEagerRelationships();

    @Query("select post from Post post left join fetch post.tags where post.slug =:slug")
    Optional<Post> findOneWithEagerRelationships(@Param("slug") String slug);

    @Query("select post from Post post left join fetch post.tags where post.slug =:slug and post.publish=true")
    Optional<Post> findOneByPublishIsTrueWithEagerRelationships(@Param("slug") String slug);

    @Query("select post from Post post left join fetch post.tags where post.id =:id")
    Optional<Post> findOneWithEagerRelationships(@Param("id") Long id);

    @Query("select post from Post post left join fetch post.tags where post.id =:id and post.publish=true")
    Optional<Post> findOneByPublishIsTrueWithEagerRelationships(@Param("id") Long id);

    Page<Post> findAllByPublishIsTrue(Pageable pageable);

    Set<Post> findAllByTags(Tags tag);

    Set<Post> findAllByCategory(Category category);

}
