package <%=packageName%>.web.rest.blog.errors;

import <%=packageName%>.web.rest.errors.ErrorConstants;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class UniqueConstraintAlertException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    private final String entityName;

    private final String errorKey;

    public UniqueConstraintAlertException(String defaultMessage, String entityName, String attributName, String errorKey) {
        this(ErrorConstants.DEFAULT_TYPE, defaultMessage, entityName, attributName, errorKey);
    }

    public UniqueConstraintAlertException(URI type, String defaultMessage, String entityName, String attributName, String errorKey) {
        super(type, defaultMessage, Status.BAD_REQUEST, null, null, null, getAlertParameters(attributName, errorKey));
        this.entityName = entityName;
        this.errorKey = errorKey;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getErrorKey() {
        return errorKey;
    }

    private static Map<String, Object> getAlertParameters(String entityName, String errorKey) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("message", "error." + errorKey);
        parameters.put("params", entityName);
        return parameters;
    }
}
