const chalk = require('chalk');
const semver = require('semver');
const fs = require('fs');
const BaseGenerator = require('generator-jhipster/generators/generator-base');
const jhipsterConstants = require('generator-jhipster/generators/generator-constants');
const packagejs = require('../../package.json');


module.exports = class extends BaseGenerator {
    get initializing() {
        return {
            init(args) {
                if (args === 'default') {
                    // do something when argument is 'default'
                }
            },
            readConfig() {
                this.jhipsterAppConfig = this.getAllJhipsterConfig();
                if (!this.jhipsterAppConfig) {
                    this.error('Can\'t read .yo-rc.json');
                }
            },
            displayLogo() {
                // it's here to show that you can use functions from generator-jhipster
                // this function is in: generator-jhipster/generators/generator-base.js
                this.printJHipsterLogo();

                // Have Yeoman greet the user.
                this.log(`\nWelcome to the ${chalk.bold.yellow('JHipster blog')} generator! ${chalk.yellow(`v${packagejs.version}\n`)}`);
            },
            checkJhipster() {
                const currentJhipsterVersion = this.jhipsterAppConfig.jhipsterVersion;
                const minimumJhipsterVersion = packagejs.dependencies['generator-jhipster'];
                if (!semver.satisfies(currentJhipsterVersion, minimumJhipsterVersion)) {
                    this.warning(`\nYour generated project used an old JHipster version (${currentJhipsterVersion})... you need at least (${minimumJhipsterVersion})\n`);
                }
            }
        };
    }

    prompting() {
        if (this.jhipsterAppConfig.clientFramework !== 'angularX') {
            this.log(chalk.red('Error! The JHipster Fortune module only works with AngularX'));
            process.exit(1);
        }
        if (this.jhipsterAppConfig.databaseType !== 'sql') {
            this.log(chalk.red('Error! The JHipster Fortune module only works with SQL databases'));
            process.exit(1);
        }
        if (this.jhipsterAppConfig.enableTranslation !== true) {
            this.log(chalk.red('Error! The JHipster Fortune module only works when translation (i18n) is enabled'));
            process.exit(1);
        }
    }

    initContext() {
        // read config from .yo-rc.json
        this.baseName = this.jhipsterAppConfig.baseName;
        this.packageName = this.jhipsterAppConfig.packageName;
        this.packageFolder = this.jhipsterAppConfig.packageFolder;
        this.clientFramework = this.jhipsterAppConfig.clientFramework;
        this.clientPackageManager = this.jhipsterAppConfig.clientPackageManager;
        this.buildTool = this.jhipsterAppConfig.buildTool;

        // use function in generator-base.js from generator-jhipster
        this.angularAppName = this.getAngularAppName();
        this.angularXAppName = this.getAngularXAppName();
    }

    writing() {
        // function to use directly template
        this.template = function (source, destination) {
            this.fs.copyTpl(
                this.templatePath(source),
                this.destinationPath(destination),
                this
            );
        };

        const javaDir = `${jhipsterConstants.SERVER_MAIN_SRC_DIR + this.packageFolder}/`;
        const resourceDir = jhipsterConstants.SERVER_MAIN_RES_DIR;
        const webappDir = jhipsterConstants.CLIENT_MAIN_SRC_DIR;
        const packageName = this.jhipsterAppConfig.packageName;
        const packageFolder = this.jhipsterAppConfig.packageFolder;

        // ##### JAVA DEPENDENCIES
        // custom package
        this.template('src/main/java/package/constant/_BlogConstant.java', `${javaDir}constant/BlogConstant.java`);

        // domain
        this.template('src/main/java/package/domain/blog/_Category.java', `${javaDir}domain/blog/Category.java`);
        this.template('src/main/java/package/domain/blog/_Comment.java', `${javaDir}domain/blog/Comment.java`);
        this.template('src/main/java/package/domain/blog/_Post.java', `${javaDir}domain/blog/Post.java`);
        this.template('src/main/java/package/domain/blog/_Tags.java', `${javaDir}domain/blog/Tags.java`);

        // repository
        this.template('src/main/java/package/repository/blog/_CategoryRepository.java', `${javaDir}repository/blog/CategoryRepository.java`);
        this.template('src/main/java/package/repository/blog/_CommentRepository.java', `${javaDir}repository/blog/CommentRepository.java`);
        this.template('src/main/java/package/repository/blog/_PostRepository.java', `${javaDir}repository/blog/PostRepository.java`);
        this.template('src/main/java/package/repository/blog/_TagsRepository.java', `${javaDir}repository/blog/TagsRepository.java`);

        // service
        this.template('src/main/java/package/service/blog/_CategoryService.java', `${javaDir}service/blog/CategoryService.java`);
        this.template('src/main/java/package/service/blog/_CommentService.java', `${javaDir}service/blog/CommentService.java`);
        this.template('src/main/java/package/service/blog/_PostService.java', `${javaDir}service/blog/PostService.java`);
        this.template('src/main/java/package/service/blog/_PostQueryService.java', `${javaDir}service/blog/PostQueryService.java`);
        this.template('src/main/java/package/service/blog/_TagsService.java', `${javaDir}service/blog/TagsService.java`);
        this.template('src/main/java/package/service/blog/dto/_CategoryDTO.java', `${javaDir}service/blog/dto/CategoryDTO.java`);
        this.template('src/main/java/package/service/blog/dto/_CommentDTO.java', `${javaDir}service/blog/dto/CommentDTO.java`);
        this.template('src/main/java/package/service/blog/dto/_PostDTO.java', `${javaDir}service/blog/dto/PostDTO.java`);
        this.template('src/main/java/package/service/blog/dto/_TagsDTO.java', `${javaDir}service/blog/dto/TagsDTO.java`);
        this.template('src/main/java/package/service/blog/dto/_PostCriteria.java', `${javaDir}service/blog/dto/PostCriteria.java`);
        this.template('src/main/java/package/service/blog/mapper/_BlogEntityMapper.java', `${javaDir}service/blog/mapper/BlogEntityMapper.java`);
        this.template('src/main/java/package/service/blog/mapper/_CategoryMapper.java', `${javaDir}service/blog/mapper/CategoryMapper.java`);
        this.template('src/main/java/package/service/blog/mapper/_CommentMapper.java', `${javaDir}service/blog/mapper/CommentMapper.java`);
        this.template('src/main/java/package/service/blog/mapper/_PostMapper.java', `${javaDir}service/blog/mapper/PostMapper.java`);
        this.template('src/main/java/package/service/blog/mapper/_TagsMapper.java', `${javaDir}service/blog/mapper/TagsMapper.java`);

        // rest
        this.template('src/main/java/package/web/rest/blog/_CategoryResource.java', `${javaDir}web/rest/blog/CategoryResource.java`);
        this.template('src/main/java/package/web/rest/blog/_CommentResource.java', `${javaDir}web/rest/blog/CommentResource.java`);
        this.template('src/main/java/package/web/rest/blog/_PostResource.java', `${javaDir}web/rest/blog/PostResource.java`);
        this.template('src/main/java/package/web/rest/blog/_TagsResource.java', `${javaDir}web/rest/blog/TagsResource.java`);
        this.template('src/main/java/package/web/rest/blog/errors/_UniqueConstraintAlertException.java', `${javaDir}web/rest/blog/errors/UniqueConstraintAlertException.java`);
        this.template('src/main/java/package/web/rest/blog/vm/_BlogSearchTermVM.java', `${javaDir}web/rest/blog/vm/BlogSearchTermVM.java`);

        // install EHCache
        if (this.jhipsterAppConfig.cacheProvider === 'ehcache') {
            this.addEntryToEhcache(`${packageName}.domain.blog.Category.class.getName()`, packageFolder);
            this.addEntryToEhcache(`${packageName}.domain.blog.Tags.class.getName()`, packageFolder);
            this.addEntryToEhcache(`${packageName}.domain.blog.Post.class.getName()`, packageFolder);
            this.addEntryToEhcache(`${packageName}.domain.blog.Post.class.getName() + ".tags"`, packageFolder);
            this.addEntryToEhcache(`${packageName}.domain.blog.Comment.class.getName()`, packageFolder);
        }

        // ##### LIQUIBASE
        this.changelogDate = this.dateFormatForLiquibase();
        this.template('src/main/resources/config/liquibase/changelog/blog/20191020132828_added_blog.xml', `${resourceDir}config/liquibase/changelog/blog/${this.changelogDate}_added_blog.xml`);
        this.addChangelogToLiquibase(`blog/${this.changelogDate}_added_blog`);

        this.template('src/main/resources/config/liquibase/fake-data/blog', `${resourceDir}config/liquibase/fake-data/blog`);

        // ##### WEBAPP
        this.template('src/main/webapp/app/blog', `${webappDir}app/blog`);

        // i18n
        if (this.jhipsterAppConfig.languages.includes('fr')) {
            this.template('src/main/webapp/i18n/fr/blog.json', `${webappDir}i18n/fr/blog.json`);
            this.addAdminElementTranslationKey('blog', 'Blog administration', 'fr');
            this.addElementTranslationKey('blogPosts', 'Articles', 'fr');
            this.addElementTranslationKey('blogCategories', 'Catégories', 'fr');
            this.addElementTranslationKey('blogTags', 'Tags', 'fr');
        }
        if (this.jhipsterAppConfig.languages.includes('en')) {
            this.template('src/main/webapp/i18n/en/blog.json', `${webappDir}i18n/en/blog.json`);
            this.addAdminElementTranslationKey('blog', 'Blog administration', 'en');
            this.addElementTranslationKey('blogPosts', 'Posts', 'en');
            this.addElementTranslationKey('blogCategories', 'Categories', 'en');
            this.addElementTranslationKey('blogTags', 'Tags', 'en');
        }
        // add BlogModule
        this.addAngularModule('Jhipster', 'Blog', 'blog', 'blog', true, this.clientFramework);

        // add on menu
        this.addElementToAdminMenu('blog/admin/dashboard', 'file-word', true, this.clientFramework, 'blog');
        this.addElementToMenu('blog', 'copy', true, this.clientFramework, 'blogPosts');
        this.addElementToMenu('blog/tags', 'tags', true, this.clientFramework, 'blogTags');
        this.addElementToMenu('blog/category', 'folder-open', true, this.clientFramework, 'blogCategories');

        // ##### DEPENDENCIES
        const fileData = this.fs.readJSON('package.json');
        // this.log(fileData);
        if (fileData && fileData.dependencies) {
            const GSIG_EDITOR = '@gsig/angular-editor-fa';
            if (!fileData.dependencies[GSIG_EDITOR]) {
                this.log(`add NPM dependency ${GSIG_EDITOR}`);
                this.addNpmDependency(GSIG_EDITOR, '0.0.1');
            }
            const FONT_A = '@fortawesome/angular-fontawesome';
            if (!fileData.dependencies[FONT_A]) {
                this.log(`add NPM dependency ${FONT_A}`);
                this.addNpmDependency(FONT_A, '0.5.0');
            }
            const FONT_SVG = '@fortawesome/fontawesome-svg-core';
            if (!fileData.dependencies[FONT_SVG]) {
                this.log(`add NPM dependency ${FONT_SVG}`);
                this.addNpmDependency(FONT_SVG, '1.2.22');
            }
            const FONT_ICO = '@fortawesome/free-solid-svg-icons';
            if (!fileData.dependencies[FONT_ICO]) {
                this.log(`add NPM dependency ${FONT_ICO}`);
                this.addNpmDependency(FONT_ICO, '5.10.2');
            }
            const NG_BOOTSTRAP = '@ng-bootstrap/ng-bootstrap';
            if (!fileData.dependencies[NG_BOOTSTRAP]) {
                this.log(`add NPM dependency ${NG_BOOTSTRAP}`);
                this.addNpmDependency(NG_BOOTSTRAP, '5.1.0');
            }
            const BOOTSTRAP = 'bootstrap';
            if (!fileData.dependencies[BOOTSTRAP]) {
                this.log(`add NPM dependency ${BOOTSTRAP}`);
                this.addNpmDependency(BOOTSTRAP, '4.3.1');
            }
        }

        // #### Advanced files
        // add security matcher for public access.
        const securityConfiguration = `src//main/java/${packageFolder}/config/SecurityConfiguration.java`;
        const data = fs.readFileSync(this.destinationPath(securityConfiguration)).toString().split('\n')
            .map((l) => {
                if (l.includes('.authorizeRequests()')) {
                    return `${l}\n            .antMatchers("/api/blog/public/**").permitAll()\r`;
                }
                return l;
            })
            .join('\n');
        fs.writeFileSync(this.destinationPath(securityConfiguration), data, (err) => {
            if (err) {
                return this.log('You need to add yourself a line');
            }
            return data;
        });
    }

    install() {
        const logMsg = `To install your dependencies manually, run: ${chalk.yellow.bold(`${this.clientPackageManager} install`)}`;

        const injectDependenciesAndConstants = (err) => {
            if (err) {
                this.warning('Install of dependencies failed!');
                this.log(logMsg);
            }
        };
        const installConfig = {
            bower: this.clientFramework === 'angular1',
            npm: this.clientPackageManager !== 'yarn',
            yarn: this.clientPackageManager === 'yarn',
            callback: injectDependenciesAndConstants
        };
        if (this.options['skip-install']) {
            this.log(logMsg);
        } else {
            this.installDependencies(installConfig);
        }
    }

    end() {
        this.log('End of blog generator');
    }
};
